from __future__ import annotations

import argparse
import os
import re
import sys

from exec_utils import exec_strict
from exec_utils.exec_strict_error import ExecStrictError


# Configuration
#
#  REMOVE_V_PREFIX=true (default false)
#     remove the 'v' from your tags
#   -> i.e. v0.1.2 will become 0.1.2
#  GENERATE_LATEST=false (default true)
#     generate latest tags
#       -> if building the main branch: 'latest'
#       -> if building a BRANCH:  $BRANCH-latest
#


def _cleanup(ref):
    if ref is None:
        return None
    ref = ref.lower()
    ref = re.sub('[^0-9a-zA-Z]+', '-', ref)
    ref = re.sub('^-+', '', ref)
    ref = re.sub('-+$', '', ref)
    return ref[:63]


def generate_tags(image_name_specs: list[str] | None):
    generate_latest = os.environ.get('GENERATE_LATEST', "true").lower() == "true"
    git_tag = os.environ.get('CI_COMMIT_TAG')
    branch = os.environ.get('CI_COMMIT_BRANCH')
    default_branch = os.environ.get('CI_DEFAULT_BRANCH')
    main_tag = _describe_git_version()

    print(f"NICE_VERSION={main_tag}")

    docker_tags = set()
    docker_tags.add(main_tag)

    if generate_latest:
        _generate_latest(branch, default_branch, docker_tags)

    if git_tag:
        docker_tags.add(git_tag)

    print("Will use following tags: ", file=sys.stderr)
    for docker_tag in docker_tags:
        print(" - " + docker_tag, file=sys.stderr)

    print(f"JIB_TAGS={','.join(docker_tags)}")

    print("\nUsage: https://gitlab.com/ergon-public/nice-version", file=sys.stderr)

    if image_name_specs is not None:
        for image_name_spec in image_name_specs:
            _generate_docker_arguments(image_name_spec, docker_tags, main_tag)


def _describe_git_version():
    remove_v_prefix = os.environ.get('REMOVE_V_PREFIX', "false").lower() == "true"

    try:
        raw_tag = exec_strict(['git', 'describe', '--always', '--tags', 'HEAD'])
        main_tag = raw_tag.strip()
        if remove_v_prefix and main_tag.startswith('v'):
            main_tag = main_tag[1:]

    except ExecStrictError as ex:
        print("Git not available or git describe produces an error, "
              f"will fallback to simple commid id. Error: {ex.stdout=!r}/{ex.stderr=!r}",
              file=sys.stderr)
        main_tag = _cleanup(_fallback_commit_id_from_ci_cd())
    return main_tag


def _fallback_commit_id_from_ci_cd():
    raw_commit_sha = os.environ.get('CI_COMMIT_SHORT_SHA',  # gitlab
                                    os.environ.get("GIT_COMMIT",  #
                                                   "development"))
    return raw_commit_sha


def _generate_latest(branch, default_branch, docker_tags):
    if branch is not None:
        if branch == default_branch:
            # be aware: we don't catch 'retry' of old pipelines
            docker_tags.add('latest')
        else:
            docker_tags.add(f'{_cleanup(branch)}-latest')


def _generate_docker_arguments(image_name_spec, docker_tags, main_tag):
    if "=" in image_name_spec:
        prefix, image_name = image_name_spec.split("=", maxsplit=1)
        prefix += "_"
    else:
        image_name = image_name_spec
        prefix = ""

    print(f"{prefix}DOCKER_IMAGE_TAG={main_tag}")
    print(f"{prefix}DOCKER_IMAGE={image_name}:{main_tag}")
    # docker build -t -t -t
    docker_push_names = []
    docker_builds_args = []
    for docker_tag in docker_tags:
        fq_docker_image = f"{image_name}:{docker_tag}"
        docker_builds_args.append(f"-t {fq_docker_image}")
        docker_push_names.append(fq_docker_image)
    print(f"{prefix}DOCKER_BUILD_ARGS={' '.join(docker_builds_args)}")
    print(f"{prefix}DOCKER_PUSH_ARGS={' '.join(docker_push_names)}")


def _make_python_version_from_git_version(some_version_str):
    # input 1.0.10-1-g9806796
    dash_parts = some_version_str.split("-")
    if len(dash_parts) < 3:
        return some_version_str

    commit_id = dash_parts.pop()
    build_no = dash_parts.pop()

    return f"{','.join(dash_parts)}.dev{build_no}+{commit_id}"


def generate_pep440_version():
    # PEP440 is quite strict, we use + to store the commit id in a local part
    # this is not guaranteed to work in all cases, please create an issue if this is
    # not sufficient for some cases.
    print(_make_python_version_from_git_version(_describe_git_version()))


if __name__ == '__main__':

    parser = argparse.ArgumentParser(
        prog='nice-version',
        description='generate a nice software version for ci/cd enviornments')

    parser.add_argument('--generate-only-pep440-version',
                        action='store_true')  # on/off flag
    parser.add_argument("image_names", nargs="*")
    args = parser.parse_args()

    if len(sys.argv) > 1:
        image_name_spec = args.image_names
    else:
        if os.environ.get('CI_REGISTRY_IMAGE'):
            image_name_spec = [os.environ.get('CI_REGISTRY_IMAGE')]
        else:
            image_name_spec = None

    if args.generate_only_pep440_version:
        generate_pep440_version()
    else:
        generate_tags(image_name_spec)
