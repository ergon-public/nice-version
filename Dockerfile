FROM python:3.10-alpine

COPY requirements.txt .
RUN apk add --no-cache git && \
      pip install -r requirements.txt
COPY nice_version.py /


