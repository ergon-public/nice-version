## Features

- Automatic generation of version numbers, depending on the git tags 
  - i.e. ``1.2.3``
  - or ``1.2.3-4-g74b10bc`` for non-tag build
- And for docker: automatic generation of 'latest' tag
  - for the default branch: using ':latest'
  - for other branches: using '$branch-latest'
- Automatic generation of arguments for `docker build` and `docker push`
  for compact gitlab pipelines

## Getting started

Add the following step to your `.gitlab-ci.yml`

```yaml
determine_version:
  stage: .pre
  image: registry.gitlab.com/ergon-public/nice-version:1.0.11
  script:
    - python /nice_version.py > build.env
  artifacts:
    reports:
      dotenv: build.env
```

This build step will generate nice versions using `git describe`. It will produce the following versions

- if the commit is tagged, it will use the name of the tag, i.e. `1.0.7`
- if the commit was pushed on a branch (inferred from GitLag Env variables) it will create a `branch_name-latest` tag
- if the commit was pushed on your default branch (i.e. `master`/`main`), (inferred from GitLab Env variables), it
  produces
  the `latest` tag
- and it will make a nice tag using `git describe`, i.e. 1.2.3-12-12abc

The system will prepare ENV variables which you can use in later steps
to use these variables.

## Configuration

| Env Variable    | Default | Description                                                                                                 |
|-----------------|---------|-------------------------------------------------------------------------------------------------------------|
| GENERATE_LATEST | true    | Should a latest tag be generated                                                                            |
| REMOVE_V_PREFIX | false   | If enabled will remove the prefix 'v', i.e. if you use the <br/>tag v0.1.0 the docker version will be 0.1.0 |

## Usage

### with Jib

To be used for [Jib](https://github.com/GoogleContainerTools/jib/tree/master/jib-gradle-plugin), i.e <br>

```yaml
- gradle jib
  -Djib.to.auth.username=${CI_REGISTRY_USER}
  -Djib.to.auth.password=${CI_REGISTRY_PASSWORD}
  -Djib.to.tags=${JIB_TAGS} # will produce 1.2.3,latest
```

### with regular gradle builds

The environment variable ``NICE_VERFSION`` contains the main version which can be directly used.

```kts
# in gradle.build.kts
group = "com.revolut.jooq"
version = System.getenv("NICE_VERSION") as String? ?: "0.0.0"
```


### For `docker build` and `docker push`

Example:

```yaml
push:
  stage: deploy
  script:
    - docker login -u $CI_REGISTRY_USER  -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker build ${DOCKER_BUILD_ARGS} .
    - for IMG in ${DOCKER_PUSH_ARGS}; do docker push $IMG; done
```

### Example for a docker multistage build with multiple components

The following example generates TAGS for both Frontend (FE) and a backend. you 
can use ``docker build --target`` to select the correct stage.


```yaml
image: docker:24

determine_version:
  stage: .pre
  image: registry.gitlab.com/ergon-public/nice-version:1.0.11
  script:
    - python /nice_version.py
      FE=my.registry.com/company/project/frontend
      BE=my.registry.com/company/project/backend
      > build.env
  artifacts:
    reports:
      dotenv: build.env

build:
  stage: build
  script:
    - docker login -u $CI_REGISTRY_USER  -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker build ${BE_DOCKER_BUILD_ARGS} --target runtime .
    - docker build ${FE_DOCKER_BUILD_ARGS} --target reverse-proxy .
    - for IMG in ${BE_DOCKER_PUSH_ARGS}; do docker push $IMG; done
    - for IMG in ${FE_DOCKER_PUSH_ARGS}; do docker push $IMG; done
```

For example when building tag ``1.2.3``the variable  ``BE_DOCKER_BUILD_ARGS`` would  
contain: 

```
   -t my.registry.com/company/project/frontend:latest \
   -t my.registry.com/company/project/frontend:1.2.3``   
```


## Arguments

You can specify an image name as an argument

- Don't specify anything, the script will determine the image name using the
  build variable `CI_REGISTRY_IMAGE`.
- You can pass the name as argument, i.e. <br> `python nice_version.py my.registry.com/my-app` <br>
  this will generate:
  ```
  DOCKER_IMAGE_TAG=1.0.7
  DOCKER_IMAGE=my.registry.com/my-app:1.0.7
  DOCKER_BUILD_ARGS=-t my.registry.com/my-app:1.0.7
  DOCKER_PUSH_ARGS=my.registry.com/my-app:1.0.7
  ```
- If you are building multiple containers within one pipeline can use the following notation
  ```shell
  python nice_version.py FE=my.registry.com/my-app-frontend BE=my.registry.com/my-app-backend
  ```
  this will generate
  ```
  FE_DOCKER_IMAGE_TAG=1.0.7
  FE_DOCKER_IMAGE=my.registry.com/my-app-frontend:1.0.7
  FE_DOCKER_BUILD_ARGS=-t my.registry.com/my-app-frontend:1.0.7
  FE_DOCKER_PUSH_ARGS=my.registry.com/my-app-frontend:1.0.7
  BE_DOCKER_IMAGE_TAG=1.0.7
  BE_DOCKER_IMAGE=my.registry.com/my-app-backend:1.0.7
  BE_DOCKER_BUILD_ARGS=-t my.registry.com/my-app-backend:1.0.7
  BE_DOCKER_PUSH_ARGS=my.registry.com/my-app-backend:1.0.7
  ```
